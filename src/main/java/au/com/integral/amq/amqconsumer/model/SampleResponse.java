package au.com.integral.amq.amqconsumer.model;

public class SampleResponse {
    String response;

    public SampleResponse() {
    }

    public SampleResponse(String response) {
        this.response = response;
    }

    public String getResponse() {
        return response;
    }

    public void setResponse(String response) {
        this.response = response;
    }
}
