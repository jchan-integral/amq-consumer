package au.com.integral.amq.amqconsumer.processor;

import au.com.integral.amq.amqconsumer.model.SampleMessage;
import au.com.integral.amq.amqconsumer.model.SampleResponse;
import org.apache.camel.Exchange;
import org.apache.camel.Processor;
import org.springframework.stereotype.Component;

@Component
public class SampleResponseProcessor implements Processor {

    @Override
    public void process(Exchange exchange) {
        SampleMessage sampleMessage = exchange.getIn().getBody(SampleMessage.class);
        if (!sampleMessage.getBody().isEmpty()) {
            exchange.getIn().setBody(new SampleResponse("Request body is: " + sampleMessage.getBody()));
        } else {
            exchange.getIn().setBody(new SampleResponse("Request body is: Hello World"));
        }

    }
}
