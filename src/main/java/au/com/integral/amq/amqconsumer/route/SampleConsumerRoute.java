package au.com.integral.amq.amqconsumer.route;

import au.com.integral.amq.amqconsumer.model.SampleMessage;
import au.com.integral.amq.amqconsumer.model.SampleResponse;
import au.com.integral.amq.amqconsumer.processor.SampleResponseProcessor;
import org.apache.camel.builder.RouteBuilder;
import org.apache.camel.component.jackson.JacksonDataFormat;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class SampleConsumerRoute extends RouteBuilder {

    @Autowired
    SampleResponseProcessor sampleResponseProcessor;

    // JSON Data Format
    JacksonDataFormat sampleMessageFormat = new JacksonDataFormat(SampleMessage.class);

    // JSON Data Format
    JacksonDataFormat jsonDataFormat = new JacksonDataFormat(SampleResponse.class);

    @Override
    public void configure() throws Exception {
        from("activemq:queue:sendMessageQueue")
                .log("Request received: ${body}")
                .unmarshal(sampleMessageFormat)
                .process(sampleResponseProcessor)
                .marshal(jsonDataFormat)
                .log("Response body is: ${body}");
    }
}
