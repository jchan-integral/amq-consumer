package au.com.integral.amq.amqconsumer;

import au.com.integral.amq.amqconsumer.model.SampleMessage;
import au.com.integral.amq.amqconsumer.processor.SampleResponseProcessor;
import org.apache.camel.CamelContext;
import org.apache.camel.EndpointInject;
import org.apache.camel.ProducerTemplate;
import org.apache.camel.builder.RouteBuilder;
import org.apache.camel.component.jackson.JacksonDataFormat;
import org.apache.camel.component.mock.MockEndpoint;
import org.apache.camel.impl.DefaultCamelContext;
import org.apache.commons.io.FileUtils;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.core.io.Resource;

import java.nio.charset.Charset;

@EnableAutoConfiguration
@SpringBootTest
class AmqConsumerApplicationTests {

	@Test
	void contextLoads() {
	}

	@Autowired
	SampleResponseProcessor sampleResponseProcessor;

	CamelContext camelContext = new DefaultCamelContext();

	// JSON Data Format
	JacksonDataFormat sampleMessageFormat = new JacksonDataFormat(SampleMessage.class);

	ProducerTemplate producerTemplate;

	@EndpointInject("mock:test")
	MockEndpoint mockEndpoint;

	@Value("classpath:testMessage.json")
	Resource testPayload;

	@Value("classpath:sampleResponse.json")
	Resource testResponse;

	String sampleRequest;
	String sampleResponse;

	@BeforeEach
	public void init() throws Exception {

		producerTemplate = camelContext.createProducerTemplate();

		sampleRequest = FileUtils.readFileToString(testPayload.getFile(), Charset.defaultCharset());

		sampleResponse = FileUtils.readFileToString(testResponse.getFile(), Charset.defaultCharset());

		camelContext.addRoutes(new RouteBuilder() {
			@Override
			public void configure() {
				from("direct:test")
						.log("Request received: ${body}")
						.unmarshal(sampleMessageFormat)
						.process(sampleResponseProcessor)
						.to(mockEndpoint)
						.end();
			}
		});

	}

	@Test
	public void setSampleResponseProcessorTest() {
		camelContext.start();
		producerTemplate.sendBody("direct:test",sampleRequest);
		mockEndpoint.expectedBodyReceived();

		camelContext.stop();
	}
}
